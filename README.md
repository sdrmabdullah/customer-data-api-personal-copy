# CustomerData API

## Note 
- Please add the full path instead of the relative path in the below places.

1. ```CustomerData.Api/HelperModules/DatabaseSeeder``` : Line 17
2. ```CustomerData.Api/appsettings.json``` : line:10


## Available end points :


- ```/Account/Login``` : Login and get access token

- ```/Api/1/Customers ``` : Get all customers

- ```/Api/1/Customers/Search?name="customerName"``` : Search customer by name

- ```/Api/1/Customers/Distance?id="cId"&lat="latitude"&lon="longitude"``` : Get distance of a customer from given location

- ```/Api/1/Customers/GroupByZip``` : Get customers grouped by zipcode

<br/>

## API Response format :

|Key|Value|
|----|-------|
|Code|Status code|
|message|Status message|
|data|payload|

<br/>

## Status codes and messages :


|Code|Message|
|----|-------|
|001|Success|
|002|Invalid or missing input parameters|
|003|Record not found|
|004|Message from error handler|

<br/>

## Requirements :
- Framework : Asp.net core 6.0

#### Dependencies :
- Auth0.AspNetCore.Authentication (1.1.0)
- Microsoft.EntityFrameworkCore (7.0.3)
- Microsoft.EntityFrameworkCore.Sqlite (7.0.3)
- Microsoft.EntityFrameWorkCore.Tools (7.0.3)

<br/>

## Running the application :

Type the command in the terminal

```
cd CustomerData.Api
```

```
dotnet run
```

<br/>

## Application Users
### 1. Admin User

Username : 
``` 
admin@admin.com
 ``` 
 Password : 
``` 
admin#123
 ``` 

 ### 2. API User

Username : 
``` 
user@user.com
 ``` 
 Password : 
``` 
apiuser123
 ``` 

## authorization

*  **Admin** : can access all of the Available endpoints
* **Api User** : except ``` Api/1/Customers ``` endpoint


## Getting access token

- make a get request to the endpoint ```
/Account/Login```
with given url parameters.

1. username : email
2. password : password

### example request

```
https://localhost:7220/Account/Login?username=admin%40admin.com&password=admin%23123
```

> include the token in the request header as bearer token.
