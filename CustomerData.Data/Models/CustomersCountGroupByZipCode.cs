﻿
namespace CustomerData.Data.Models
{
    public class CustomersCountGroupByZipCode
    {
        public int zipCode { get; set; }
        public int NumberOfCustomers { get; set; }
    }
}
