﻿namespace CustomerData.Data.Models
{
    public class ApiResponse
    {
        public string Code { get; set; }
        public string? Message { get; set; }
        public dynamic? Data { get; set; }
    }

    public class Messages {
        public static string m001 = "Success.";
        public static string m002 = "Invalid or missing input parameters.";
        public static string m003 = "Record not found.";
    }
}
