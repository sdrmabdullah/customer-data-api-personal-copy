﻿using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace CustomerData.Data.Models
{
    public class Customer
    {
        [Key, Required]
        public string _id { get; set; }
        public int? index { get; set; }
        public int? age { get; set; }
        public string? eyeColor { get; set; }
        public string name { get; set; }
        public string? gender { get; set; }
        public string? company { get; set; }
        public string? email { get; set; }
        public string? phone { get; set; }
        public Address? address { get; set; }
        public int? addressid { get; set; }
        public string? about { get; set; }
        public string? registered { get; set; }
        public string? latitude { get; set; }
        public string? longitude { get; set; }
    }

    public class Address {
        [Key]
        public int id { get; set; }
        public int? number { get; set; }
        public string? street { get; set; }
        public string? city { get; set; }
        public string? state { get; set; }
        public int? zipCode { get; set; }
    }
}
