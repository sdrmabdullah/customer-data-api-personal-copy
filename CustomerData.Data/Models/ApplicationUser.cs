﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace CustomerData.Data.Models
{
    public class ApplicationUser
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string password { get; set; }
        public string Role { get; set; }
        public string? ProfilePic { get; set; }
    }
}
