﻿using CustomerData.Data.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace CustomerData.Data
{
    // creating applicationDbContext from DbContext
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> option) : base(option)
        {
        }

        // customers table
        public DbSet<Customer> Customers { get; set; }

        // addresses table
        public DbSet<Address> Addresses { get; set; }

        // Application users table
        public DbSet<ApplicationUser> ApplicationUsers { get; set; } 

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Address>().Property(c => c.id).ValueGeneratedOnAdd();
            modelBuilder.Entity<ApplicationUser>().Property(u => u.Id).ValueGeneratedOnAdd();
        }
    }
}
