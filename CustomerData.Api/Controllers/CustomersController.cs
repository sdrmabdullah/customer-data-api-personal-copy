﻿using CustomerData.Data;
using CustomerData.Data.Models;
using CustomerData.Api.HelperModules;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace CustomerData.Api.Controllers
{
    [Route("/Api/1")]
    [Authorize(Roles = "Admin, User")]
    public class CustomersController : Controller
    {
        // creating an instance of applicationDb context
        private ApplicationDbContext _context;
        public CustomersController(ApplicationDbContext context)
        {
            _context = context;
        }


        // GET Customers
        // get all customers list 
        [HttpGet]
        [Route("Customers")]
        [Authorize(Roles = "Admin")]
        public async Task<ApiResponse> Customers()
        {
            var response = new ApiResponse();
            try
            {
                var customers = _context.Customers.ToArray();
                foreach (var customer in customers)
                {
                    customer.address = await _context.Addresses.FirstOrDefaultAsync(a => a.id == customer.addressid);
                }
                response.Code = "001";
                response.Message = Messages.m001;
                response.Data = customers;

            }
            catch (Exception ex)
            {
                response.Code = "004";
                response.Message = ex.Message;
                response.Data = null;
            }
            return response;
        }


        // GET Customers/Details?id
        // get single customer detail
        [HttpGet]
        [Route("Customers/Details")]
        public async Task<ApiResponse> SingleCustomer(string id)
        {
            var response = new ApiResponse();
            try
            {
                // if id is not null and customer exists with the id
                if (!string.IsNullOrEmpty(id) && _context.Customers.Any(c => c._id == id))
                {
                    var customer = await _context.Customers.FirstOrDefaultAsync(s => s._id == id);
                    var address = await _context.Addresses.FirstOrDefaultAsync(a => a.id == customer.addressid);
                    customer.address = address;
                    response.Code = "001";
                    response.Message = Messages.m001;
                    response.Data = customer;
                }
                else {
                    response.Code = "002";
                    response.Message = Messages.m002;
                    response.Data = null;
                }
            }
            catch (Exception ex)
            {
                response.Code = "004";
                response.Message = ex.Message;
                response.Data = null;
            }
            return response;
        }


        // GET Customers/Search?name
        // search customer by name
        [HttpGet]
        [Route("Customers/Search")]
        public async Task<ApiResponse> SearchResult(string name)
        {
            var response = new ApiResponse();

            try
            {
                // if id is not null and customer exists with the id
                if (!string.IsNullOrEmpty(name) && _context.Customers.Any(c=> c.name.ToLower() == name.ToLower()))
                {
                    var customer = await _context.Customers.FirstOrDefaultAsync(c => c.name.ToLower() == name.ToLower());
                    var address = await _context.Addresses.FirstOrDefaultAsync(a => a.id == customer.addressid);
                    customer.address = address;
                    response.Code = "001";
                    response.Message = Messages.m001;
                    response.Data = customer;
                }
                else
                {
                    response.Code = "002";
                    response.Message = Messages.m002;
                    response.Data = null;
                }
            }
            catch (Exception ex)
            {
                response.Code = "004";
                response.Message = ex.Message;
                response.Data = null;
            }
            return response;
        }


        // POST: Customers/EditCustomer?id
        // edit/update customer
        [HttpPost]
        [Route("Customers/EditCustomer")]
        public async Task<ApiResponse> EditCustomer(string id, [FromBody] Customer newData)
        {
            var response = new ApiResponse();

            try
            {
                // if id is not null and customer exists with the id
                if (!string.IsNullOrEmpty(id) && _context.Customers.Any(c=> c._id == id))
                {
                    var existingCustomer = await _context.Customers.FirstOrDefaultAsync(c => c._id == id);

                    // update modified fields from incoming newData
                    var properties = typeof(Customer).GetProperties();
                    foreach (var property in properties)
                    {
                        // exclude id property
                        if (property.Name.ToLower() != "_id") {
                            var newValue = property.GetValue(newData);
                            if (newValue != null)
                            {
                                property.SetValue(existingCustomer, newValue);
                            }
                        }
                    }

                    // update with new values
                    _context.Update(existingCustomer);
                    await _context.SaveChangesAsync();
                    response.Code = "001";
                    response.Message = Messages.m001;
                    response.Data = existingCustomer;
                }
                else
                {
                    response.Code = "002";
                    response.Message = Messages.m002;
                    response.Data = null;
                }
            }
            catch (Exception ex)
            {
                response.Code = "004";
                response.Message = ex.Message;
                response.Data = null;
            }

            return response;
        }

        //GET: Customers/GroupByZip
        // get customers grouped by zip code
        [HttpGet]
        [Route("Customers/GroupByZip")]
        public ApiResponse GroupByZip()
        {
            string query = $@"SELECT a.zipCode, COUNT(c._id) AS NumberOfCustomers 
                            FROM Customers c 
                            INNER JOIN Addresses a ON c.addressid = a.id 
                            GROUP BY a.zipCode;";

            var response = new ApiResponse();
            // as an array list
            var results = new List<CustomersCountGroupByZipCode>();

            try
            {
                using (var command = _context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = query;
                    command.CommandType = System.Data.CommandType.Text;
                    _context.Database.OpenConnection();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var zipCode = reader.GetInt32(0);
                            var numberOfCustomers = reader.GetInt32(1);
                            results.Add(new CustomersCountGroupByZipCode { zipCode = zipCode, NumberOfCustomers = numberOfCustomers });
                        }
                    }
                }
                response.Code = "001";
                response.Message = Messages.m001;
                response.Data = results;
            }
            catch (Exception ex)
            {
                response.Code = "004";
                response.Message = ex.Message;
                response.Data = null;
            }

            return response;
        }


        // GET: Customers/Distance/id,lat,lon
        // get customer's distance from given coordinates 
        [HttpGet]
        [Route("Customers/Distance")]
        public async Task<ApiResponse> Distance(string id, string lat, string lon)
        {
            string distance;
            var response = new ApiResponse();

            if (id != null && _context.Customers.Any(c => c._id == id))
            {
                var customer = await _context.Customers.FirstOrDefaultAsync(c => c._id == id);

                distance = CalcDistance.findDistance(customer.latitude, customer.longitude, lat, lon);
                response.Code = "001";
                response.Message = Messages.m001;
                response.Data = distance;
            }
            else {
                response.Code = "002";
                response.Message = Messages.m002;
                response.Data = null;
            }
            return response;
        }

    }
}
