﻿using CustomerData.Api.HelperModules;
using CustomerData.Data;
using CustomerData.Data.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CustomerData.Api.Controllers
{
    [Route("/Account")]
    public class AccountController : Controller
    {
        private ApplicationDbContext _context;
        public AccountController(ApplicationDbContext context)
        {
            _context = context;
        }
        [HttpGet]
        [Route("Login")]
        public async Task<ApiResponse> login(string username, string password)
        {
            var response = new ApiResponse();

            if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(username) && _context.ApplicationUsers.Any(u => u.Email == username && u.password == password))
            {
                var user = await _context.ApplicationUsers.FirstOrDefaultAsync(u => u.Email == username);
                var token = CreateToken.Create(user);
                response.Code = "001";
                response.Message = Messages.m001;
                response.Data = new
                {
                    accessToken = token,
                };
            }
            else
            {
                response.Code = "002";
                response.Message = Messages.m002;
                response.Data = null;
            }
            return response;
        }
    }
}
