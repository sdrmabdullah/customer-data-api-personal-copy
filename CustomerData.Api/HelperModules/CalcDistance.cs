﻿namespace CustomerData.Api.HelperModules
{
    // This class has findDistance method to calculate the distance using latitude and longitude
    // This class is used by CustomersController's GetDistance action
    public class CalcDistance
    {
        public static string findDistance(string customerLatitude, string customerLongitude, string baseLatitude, string baseLongitude)
        {
            double baseLat = double.Parse(baseLatitude);
            double baseLon = double.Parse(baseLongitude);
            double customerLat = double.Parse(customerLatitude);
            double customerLon = double.Parse(customerLongitude);

            if (customerLat == baseLat && customerLon == baseLon)
            {
                return "0 KM";
            }
            else
            {
                double theta = baseLon - customerLon;
                double dist = Math.Sin(deg2rad(customerLat)) * Math.Sin(deg2rad(baseLat)) + Math.Cos(deg2rad(baseLat)) * Math.Cos(deg2rad(customerLat)) * Math.Cos(deg2rad(theta));
                dist = Math.Acos(dist);
                dist = rad2deg(dist);
                dist = dist * 60 * 1.1515;

                dist = dist * 1.609344;

                return Convert.ToInt32(dist).ToString() + " KM";
            }
        }

        static double deg2rad(double deg)
        {
            return deg * Math.PI / 180.0;
        }
        static double rad2deg(double rad)
        {
            return rad / Math.PI * 180.0;
        }
    }
}
