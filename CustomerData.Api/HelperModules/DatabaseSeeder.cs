﻿using CustomerData.Data;
using CustomerData.Data.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace CustomerData.Api.HelperModules
{
    public static class DatabaseSeeder
    {
        public static void Initialize(ApplicationDbContext context) {

            // try to insert initial data
            try
            {
                if (!context.Customers.Any())
                {
                    var json = File.ReadAllText("C:\\Users\\Fidenz\\Documents\\Visual Studio 2022\\Projects\\CustomerData\\CustomerData.Data\\Data\\UserData.json");
                    var customers = JsonConvert.DeserializeObject<Customer[]>(json);

                    context.Customers.AddRange(customers);
                    context.SaveChanges();
                }
                if (!context.ApplicationUsers.Any())
                {
                    var admin = new ApplicationUser();
                    admin.Email = "admin@admin.com";
                    admin.Name = "admin";
                    admin.password = "admin#123";
                    admin.Role= "Admin";

                    var apiUser = new ApplicationUser();
                    apiUser.Email = "user@user.com";
                    apiUser.Name = "api user";
                    apiUser.password = "apiuser123";
                    apiUser.Role = "User";

                    context.ApplicationUsers.Add(admin);
                    context.ApplicationUsers.Add(apiUser);
                    context.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
